# Codebase for Code Search #

Within this archive you find all the raw and preprocessed data for the article "Simplifying Deep-Learning-Based Model for Code Search" by Chao Liu, Xin Xia, David Lo, Ahmed E. Hassan, and Shanping Li for currently under review at ACM Transactions on Software Engineering and Methodology. The aim of this replication package is to allow other researchers to replicate our results with minimal effort, as well as to provide additional results that could not be included in the article directly.

### Data Source ###

41,025 Java repositories from GitHub created from Jul. 2016 to Dec. 2018 with more than five stars.

### Data Contents ###

* You can get the full data from the [link](https://pan.baidu.com/s/1PzHLQ4gmjFMEOr0FTfjCkw) with permenant key (0s7i)
* codebase_repos.csv lists the index, user name, repo name for each repositories obtained from GitHub.
* codebase_java/ contains all the source code of repositories.
* codebase_parse/ contains all the method components (e.g., method name, Javadoc, APIs, and etc.) parsed from codebase_java/ by our tool [Janalyzer](https://github.com/liuchaoss/janalyzer).

